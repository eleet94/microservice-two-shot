import React, { useState, useEffect } from 'react';


function ShoeForm({ loadShoes }) {
    const [model, setModel] = useState('');
    const [color, setColor] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [picture, setPicture] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);
    console.log(bins);

    useEffect(() => {
        async function getBins() {
          const url = 'http://localhost:8100/api/bins/';
          const response = await fetch(url);
          if (response.ok) {
              const data = await response.json();
              setBins(data.bins);
          }
        }
        getBins();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            model,
            color,
            manufacturer,
            picture,
            bin: Number(bin),
        };

        console.log(data);

    const shoeUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const response = await fetch(shoeUrl, fetchConfig);
      if (response.ok) {
          const newShoe = await response.json();
          console.log(newShoe);
          loadShoes();
          setModel('');
          setColor('');
          setManufacturer('');
          setPicture('');
          setBin('');
      }

}

function handleChangeModel(event) {
    const value = event.target.value;
    setModel(value);
}
function handleChangeColor(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handleChangeManufacturer(event) {
    const value = event.target.value;
    setManufacturer(value);
  }

  function handleChangePicture(event) {
    const value = event.target.value;
    setPicture(value);
  }

  function handleChangeBin(event) {
    const value = event.target.value;
    setBin(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Shoe</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeModel} value={model} placeholder="Model" required type="text" name="model" id="model" className="form-control" />
              <label htmlFor="name">Model</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="starts">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeManufacturer} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufaturer" id="manufacturer" className="form-control" />
              <label htmlFor="ends">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangePicture} value={picture} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
              <label htmlFor="ends">Picture</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeBin} value={bin} required name="bin" id="bin" className="form-select">
                <option value="">Choose Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoeForm;
