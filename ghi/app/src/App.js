import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import { useEffect, useState } from 'react';
import ShoeForm from './ShoeForm';
import ListShoes from './ListShoes';
import HatForm from './Hatform';
import ListHats from './ListHats';

function App() {
  const [shoes, setShoes] = useState([]);
  const [bins, setBins] = useState([]);
  const [hats, setHats] = useState([]);


  async function loadShoes() {
    const url = "http://localhost:8080/api/shoes/"
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes);
      console.log("shoes:", data.shoes)
    } else {
      console.error(response);
    }
  }

  async function loadBins() {
    const response = await fetch('http://localhost:8100/api/bins/');
    if (response.ok) {
      const data = await response.json();
      setBins(data.bins);
    } else {
      console.error(response);
    }
  }

  async function loadHats() {
    const url = "http://localhost:8090/api/hats/"
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setHats(data.hats);
      console.log("hats:", data.hats)
    } else {
      console.error(response);
    }
  }


  useEffect(() => {
    loadShoes();
    loadBins();
    loadHats();

    console.log("Use effect flash!")
  }, []);

  if (shoes === undefined) {
    return null;
  }
  if (bins === undefined) {
    return null;
  }
  if (hats === undefined) {
    return null;
  }
  console.log("React rendered!");
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element= {<ListShoes bins={bins} shoes={shoes} />} />
            <Route path='new' element= {<ShoeForm loadBins={loadBins} loadShoes={loadShoes} />} />
          </Route>
          <Route path="hats">
            <Route index element= {<ListHats hats={hats} />} />
            <Route path='new' element= {<HatForm loadHats={loadHats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
