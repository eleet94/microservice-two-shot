function ListHats(props) {

    const deleteHat = async(hat) => {
        try {
            const deleted = hat.id
            const hatUrl = `http://localhost:8080/api/hats/${deleted}`;
            const fetchConfig = {
                method: "delete"
            };
            const response = await fetch(hatUrl, fetchConfig);
            if (response.ok) {
                console.log("hat deleted")
                props.getHats()
            };
        } catch (e) {
            console.log(e)
        }
    }
    return (
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Fabric</th>
            <th>Style Name</th>
            <th>Color</th>
            <th>Location</th>
          </tr>
        </thead>
        <tbody>
          {props.hats.map(hat => {
            return (
              <tr key={hat.id}>
                <td>{ hat.fabric }</td>
                <td>{ hat.style_name }</td>
                <td>{ hat.color }</td>
                <td>{ hat.location }</td>
                <td>
                <button onClick={() => deleteHat(hat)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }

  export default ListHats;
