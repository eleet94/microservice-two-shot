import React, { useState, useEffect } from 'react';


function HatForm({ loadHats }) {
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [style_name, setStyle_Name] = useState('');
    const [picture_url, setPicture_Url] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);


    useEffect(() => {
        async function getLocations() {
            const url = 'http://localhost:8100/api/locations/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations);
            }
        }
        getLocations();
    }, []);

    async function handleSubmit(event) {
        event.preventDefault();
        const data = {
            fabric,
            style_name,
            color,
            picture_url,
            location,
        };

    const hatUrl = 'http://localhost:8080/api/hats/';
    const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application.json',
        },
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
        const newHat = await response.json();
        console.log(newHat);
        loadHats();
        setFabric('');
        setColor('');
        setStyle_Name('');
        setPicture_Url('');
        setLocation('');
    }

}

function handleChangeFabric(event) {
    const value = event.target.value;
    setFabric(value);
}
function handleChangeColor(event) {
    const value = event.target.value;
    setColor(value);
  }

  function handleChangeStyle_Name(event) {
    const value = event.target.value;
    setStyle_Name(value);
  }

  function handleChangePicture_Url(event) {
    const value = event.target.value;
    setPicture_Url(value);
  }

  function handleChangeLocation(event) {
    const value = event.target.value;
    setLocation(value);
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add a new Hat</h1>
          <form onSubmit={handleSubmit} id="create-hat-form">
            <div className="form-floating mb-3">
              <input onChange={handleChangeFabric} value={fabric} placeholder="Fabric" required type="text" name="" id="fabric" className="form-control" />
              <label htmlFor="name">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="starts">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangeStyle_Name} value={style_name} placeholder="Style Name" required type="text" name="style name" id="style name" className="form-control" />
              <label htmlFor="ends">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChangePicture_Url} value={picture_url} placeholder="Picture" required type="url" name="picture" id="picture" className="form-control" />
              <label htmlFor="ends">Picture</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChangeLocation} value={location} required name="location" id="location" className="form-select">
                <option value="">Choose Location</option>
                {locations.map(location => {
                  return (
                    <option key={location.href} value={location.href}>{location.closet_name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatForm;
