# Generated by Django 4.0.3 on 2023-06-05 02:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0006_alter_hats_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='hats',
            options={'ordering': ('style_name', 'color')},
        ),
    ]
