from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()




class Hats(models.Model):
    fabric = models.CharField(max_length=100, null=True)
    style_name = models.CharField(max_length=100, null=True)
    color = models.CharField(max_length=100, null=True)
    picture_url = models.CharField(max_length=100, null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.PROTECT,
        null=True,
    )

    def get_api_url(self):
        return reverse("api_list_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.style_name}"

    class Meta:
        ordering = ("style_name", "color")
