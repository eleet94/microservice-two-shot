from django.urls import path
from hats_rest.api_views import api_hat_detail, api_list_hats

urlpatterns = [
   
    path('hats/<int:pk>', api_hat_detail, name="api_hat_detail"),
    path('hats/', api_list_hats, name="api_list_hats"),
]
