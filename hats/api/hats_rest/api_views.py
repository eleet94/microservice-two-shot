from django.http import JsonResponse
from .models import Hats, LocationVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
# Create your views here

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "id",
        "closet_name",
        "location_number",
        "location_size",
        "import_href",
    ]


class HatListEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }




@require_http_methods([""])



@require_http_methods(["DELETE", "GET"])
def api_hat_detail(request, pk):
    if request.method == "GET":
        try:
            hat = Hats.objects.get(id=pk)
            return JsonResponse(
                {"hat": hat},
                encoder=HatDetailEncoder,
                safe=False,
            )
        except Hats.DoesNotExist:
         return JsonResponse(
                {"mesage": "Hat doesn't exist!"},
                status=400)
    else:
        count, _ = Hats.objects.get(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        id = content["location"]
        try:
            location = LocationVO.objects.get(id=id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"Error": "Invalid hat id"},
                status=400
            )
        hat = Hats.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False
        )


@require_http_methods(["GET"])
def api_list_locationvos(request):
    locationvos = LocationVO.objects.all()
    return JsonResponse(
        {"locationvos": locationvos},
        encoder=LocationVOEncoder,
        safe=False
    )
