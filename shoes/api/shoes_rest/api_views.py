
from django.http import JsonResponse
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder


# Create your views here


class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "id",
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOEncoder(),
    }




@require_http_methods(["DELETE", "GET"])
def api_shoe_detail(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"mesage": "Shoe doesn't exist!"},
                status=400)
    else:
        count, _ = Shoe.objects.get(id=pk).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
            safe=False,
        )
    if request.method == "POST":
        content = json.loads(request.body)
        id = content["bin"]

        print("LOOK HERE")
        print(id)

        try:
            bin = BinVO.objects.get(id=id)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"Error": "Invalid bing id"},
                status=400
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )


@require_http_methods(["GET", "POST"])
def api_list_binvos(request):
    if request.method == "GET":
        bins = BinVO.objects.all()
        return JsonResponse(
            {"bins": bins},
            encoder=BinVOEncoder,
            safe=False
        )
    else:
        content = json.loads(request.body)
        bin = BinVO.objects.create(**content)
        return JsonResponse(
            bin,
            encoder=BinVOEncoder,
            safe=False,
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_binvo(request, pk):
    """
    Single-object API for the Bin resource.

    GET:
    Returns the information for a Bin resource based
    on the value of pk
    {
        "id": database id for the bin,
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
        "href": URL to the bin,
    }

    PUT:
    Updates the information for a Bin resource based
    on the value of the pk
    {
        "closet_name": bin's closet name,
        "bin_number": the number of the bin,
        "bin_size": the size of the bin,
    }

    DELETE:
    Removes the bin resource from the application
    """
    if request.method == "GET":
        try:
            bin = BinVO.objects.get(id=pk)
            return JsonResponse(
                bin,
                encoder=BinVOEncoder,
                safe=False
            )
        except BinVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            bin = BinVO.objects.get(id=pk)
            bin.delete()
            return JsonResponse(
                bin,
                encoder=BinVOEncoder,
                safe=False,
            )
        except BinVO.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else: # PUT
        try:
            content = json.loads(request.body)
            bin = BinVO.objects.get(id=pk)

            props = ["closet_name", "bin_number", "bin_size"]
            for prop in props:
                if prop in content:
                    setattr(bin, prop, content[prop])
            bin.save()
            return JsonResponse(
                bin,
                encoder=BinVOEncoder,
                safe=False,
            )
        except BinVO.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
