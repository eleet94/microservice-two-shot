
from django.urls import path
from .api_views import api_list_shoes, api_shoe_detail, api_list_binvos

urlpatterns = [
    path('shoes/', api_list_shoes, name="api_list_shoes"),
    path('shoes/<int:pk>', api_shoe_detail, name="api_shoe_detail"),
    path('bins/', api_list_binvos, name="api_list_binvos")
]
