# Wardrobify

Team:

* Edward: shoes microservice
* Jacob: hats microservice

## Design

We are creating microservices which build up our wardrobe with new objects, which in our case, are hats and shoes. Our hats and shoes data will be stored on two separate containers within the wardrobe named locations and bins respectively. We will access data accross services with RESTful APIs and render that data dynamically via webpages with React. By utilizing stately functions and React router, we establish the necessary groundwork for scaling up our application to receive more entries and objects.


## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The shoes microservice allows users to create and store shoes within "bins" which originate from the wardrobe microservice. We fetch the bin data with value objects which we can then associate with the correct shoe. Shoes can be viewed on a table with detailed headers to help users keep track of their wardrobe. From that same list, there's a button beside each shoe which allows users to delete entries.

I realized too late that bins had to come from BinVOs instead of from the wardrobe and tried unsuccessfully to rewrite portions of code using new BinVO views. I've managed to create bins on both microservices but was unable to make more than one bin on the BinVO endpoint which I believe was because I couldn't import the wardrobe bin href properly.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.


I created two model classes: LocationVo and Hats. They each contain the attributes for the hat and location(example: fabric). It gets integrated through the view functions at the from/import statement. I wasn't able to get the location to render nor could I delete the hat data.
